# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4

PortSystem          1.0
PortGroup           github 1.0
PortGroup           python 1.0

github.setup        tazzben WW 1.7

categories          science education
platforms           darwin
maintainers         unomaha.edu:bosmith
license             MIT
supported_archs     noarch

description         A command line tool to disaggregate pre and post test responses into Walstad and Wagner learning types

long_description    In the Spring of 2016, Walstad and Wagner released a paper suggesting that the pretest/posttest delta is insufficient in assessing learning outcomes. However, performing such a disaggregation is time intensive, especially if the questions appear in a different location (or order) on the pre and post test.  WW_out is a command line tool that makes this disaggregation easy. It uses four CSV files to generate outcomes by question and student.

checksums           rmd160  ba30219ea35fc0a773fb1261e77c664d730ecef4 \
                    sha256  2873483cd3682c29f8423a8f5d590c5f7b6d3d9094cc651a84968b1db2ba74f9

python.default_version  27

depends_build       port:py${python.version}-setuptools
depends_lib-append  port:py${python.version}-pandas
